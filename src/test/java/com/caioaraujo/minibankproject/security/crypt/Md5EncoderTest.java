package com.caioaraujo.minibankproject.security.crypt;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
public class Md5EncoderTest {

    private final String password = "123456";
    private final String passwordEncoded = "E10ADC3949BA59ABBE56E057F20F883E";
    private final Md5PasswordEncoder encoder = new Md5PasswordEncoder();

    @Test
    public void testEncodeNotReturnNull() {
        Assert.notNull(encoder.encode(password), "Encoder not return null value");
    }

    @Test
    public void testEncodeCryptsRight() {
        String hashEncoded = encoder.encode(password);
        Assert.isTrue(passwordEncoded.equals(hashEncoded), "Encode MD5 is right!");
    }

    @Test
    public void testMatchesCrypt() {
        Assert.isTrue(new Md5PasswordEncoder().matches(password, passwordEncoded), "Matches is right");
    }

}
