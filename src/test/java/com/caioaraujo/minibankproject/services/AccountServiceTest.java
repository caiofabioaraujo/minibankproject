package com.caioaraujo.minibankproject.services;


import com.caioaraujo.minibankproject.entities.Account;
import com.caioaraujo.minibankproject.repositories.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AccountServiceTest {

    @MockBean
    private AccountRepository repository;

    @Autowired
    private AccountService accountService;

    @Before
    public void setUp() {
        BDDMockito.given(this.repository.save(Mockito.any(Account.class))).willReturn(new Account());
        BDDMockito.given(this.repository.getOne(Mockito.anyString())).willReturn(new Account());
    }

    @Test
    public void testGetAccountByNumberAccount() {
        Account account = this.accountService.getAccountByNumberAccount(new String());
        Assert.notNull(account, "Services find by account number is ok!");
    }
}
