package com.caioaraujo.minibankproject.entities.enums;

public enum EnumTransactionType {
    CREDIT,
    DEBIT
}
