package com.caioaraujo.minibankproject.entities.enums;

public enum EnumOperationType {
    SAKE,
    DEPOSIT,
    TRANSFER
}
