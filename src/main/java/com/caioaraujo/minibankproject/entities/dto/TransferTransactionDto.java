package com.caioaraujo.minibankproject.entities.dto;

import com.caioaraujo.minibankproject.entities.enums.EnumOperationType;
import lombok.Data;

@Data
public class TransferTransactionDto extends TransactionDTO {

    private String numberAccountFrom;
    private String numberAccountDestination;

    public TransferTransactionDto() {
        setOperationType(EnumOperationType.TRANSFER);
    }

}
