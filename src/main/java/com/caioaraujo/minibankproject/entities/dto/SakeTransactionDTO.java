package com.caioaraujo.minibankproject.entities.dto;

import com.caioaraujo.minibankproject.entities.OperationTransaction;
import com.caioaraujo.minibankproject.entities.enums.EnumOperationType;
import com.caioaraujo.minibankproject.entities.enums.EnumTransactionType;
import lombok.Data;

@Data
public class SakeTransactionDTO extends TransactionDTO {

    String atmRegistration;

    public SakeTransactionDTO() {
        setOperationType(EnumOperationType.SAKE);
        setTransactionType(EnumTransactionType.DEBIT);
    }
}
