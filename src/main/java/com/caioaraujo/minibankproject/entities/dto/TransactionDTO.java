package com.caioaraujo.minibankproject.entities.dto;

import com.caioaraujo.minibankproject.entities.enums.EnumOperationType;
import com.caioaraujo.minibankproject.entities.enums.EnumTransactionType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public abstract class TransactionDTO {

    private Integer transactionIdentifier;
    private String numberAccount;
    private String identifyDocument;
    private BigDecimal transactionValue;
    private EnumOperationType operationType;
    private EnumTransactionType transactionType;
    private Date transactionDate;

}
