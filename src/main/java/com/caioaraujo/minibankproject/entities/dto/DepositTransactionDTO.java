package com.caioaraujo.minibankproject.entities.dto;

import com.caioaraujo.minibankproject.entities.enums.EnumOperationType;
import com.caioaraujo.minibankproject.entities.enums.EnumTransactionType;
import lombok.Data;

@Data
public class DepositTransactionDTO extends TransactionDTO {

    private String envelopeControl;
    private String atmRegistration;
//
//    public DepositTransactionDTO() {
//        setOperationType(EnumOperationType.DEPOSIT);
//        setTransactionType(EnumTransactionType.CREDIT);
//    }
}
