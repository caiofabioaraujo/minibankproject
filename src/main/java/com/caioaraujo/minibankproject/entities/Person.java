package com.caioaraujo.minibankproject.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
public class Person implements Serializable {

    @Id
    @Column(length = 11)
    private String identifyDocument;
    private String name;
    private boolean principalAccount;
    @ManyToOne
    @JoinColumn(name = "number_account")
    private Account account;

}
