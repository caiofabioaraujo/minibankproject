package com.caioaraujo.minibankproject.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
public class Account implements Serializable {

    @Id
    @Column(length = 8)
    private String numberAccount;
    private BigDecimal statement;
    @OneToMany(mappedBy = "account")
    private List<Person> clientsAccount;
    private boolean active;

}
