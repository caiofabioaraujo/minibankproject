package com.caioaraujo.minibankproject.entities;

import com.caioaraujo.minibankproject.entities.enums.EnumTransactionType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
public class OperationTransaction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "number_account")
    private Account account;
    @ManyToOne
    @JoinColumn(name = "identify_document")
    private Person person;
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private AccountOperation operation;
    @Enumerated(EnumType.STRING)
    private EnumTransactionType transactionType;
    private BigDecimal oldAccountStatement;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

}
