package com.caioaraujo.minibankproject.security.dto;

import com.caioaraujo.minibankproject.entities.Credentials;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;

public class UserJWTFactory {

    private UserJWTFactory() {}

    /**
     * We'll to utilized Credentials object as object to create the User
     *
     * @param credentials
     * @return
     */
    public static UserJWT create(Credentials credentials) {
        return new UserJWT(credentials.getAccount().getNumberAccount(), credentials.getPassword(),
                credentials.getAccount().isActive(), mapToGrantedAuthorization());
    }

    /**
     * We'll create granted authorization per user.
     *
     * TODO creates list authorization for project
     * @return
     */
    private static Collection<? extends GrantedAuthority> mapToGrantedAuthorization() {
        return new ArrayList<>();
    }
}
