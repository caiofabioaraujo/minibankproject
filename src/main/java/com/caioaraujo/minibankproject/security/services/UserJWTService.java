package com.caioaraujo.minibankproject.security.services;

import com.caioaraujo.minibankproject.entities.Credentials;
import com.caioaraujo.minibankproject.security.dto.UserJWTFactory;
import com.caioaraujo.minibankproject.services.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserJWTService implements UserDetailsService {

    private CredentialsService credentialsService;

    @Autowired
    public UserJWTService(CredentialsService credentialsService) {
        this.credentialsService = credentialsService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<Credentials> optionalCredentials = Optional.ofNullable(credentialsService.findByNumberAccount(username));
        if (optionalCredentials.isPresent())
            return UserJWTFactory.create(optionalCredentials.get());
        throw new UsernameNotFoundException("Access denied. Sorry but we don't granted access for you!");
    }

}
