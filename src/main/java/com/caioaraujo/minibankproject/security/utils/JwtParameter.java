package com.caioaraujo.minibankproject.security.utils;

public class JwtParameter {

    public static final String TOKEN_HEADER = "Authorization";
    public static final String BEARER_PREFIX = "Bearer=";

    private JwtParameter(){}

}
