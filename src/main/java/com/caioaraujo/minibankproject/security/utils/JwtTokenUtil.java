package com.caioaraujo.minibankproject.security.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class JwtTokenUtil {

    static final String CLAIM_KEY_USERNAME = "sub";
    static final String CLAIM_KEY_ROLE = "role";
    static final String CLAIM_KEY_AUDIENCE = "audience";
    static final String CLAIM_KEY_CREATED = "created";

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    /**
     * Get username(person.identify_document) of JWT token.
     *
     * @param token
     * @return String
     */
    public String getUsernameFromToken(String token) {
        String identifyDocument = null;
        try {
            Optional<Claims> claims = getClaimsFromToken(token);
            if (claims.isPresent())
                identifyDocument = claims.get().getSubject();
        } catch (Exception e) {
            // TODO implements new method to log
        }
        return identifyDocument;
    }

    /**
     * Return expiration date fo JWT token
     *
     * @param token
     * @return Date
     */
    public Date getExpirationDateFromToken(String token) {
        Date expirationDate = null;
        try {
            Optional<Claims> claims = getClaimsFromToken(token);
            if (claims.isPresent())
                expirationDate = claims.get().getExpiration();
        } catch (Exception e) {
            //TODO Create ERROR Handling
        }
        return expirationDate;
    }

    /**
     * Create a new token (or refresh)
     *
     * @param token
     * @return String
     */
    public String refreshToken(String token) {
        String refreshedToken = null;
        try {
            Optional<Claims> claims = getClaimsFromToken(token);
            if (claims.isPresent()) {
                claims.get().put(CLAIM_KEY_CREATED, new Date());
                refreshedToken = generateToken(claims.get());
            }
        } catch (Exception e) {
            //TODO Create ERROR Handling
        }
        return refreshedToken;
    }

    /**
     * Verify and return a valid JWT token
     *
     * @param token
     * @return boolean
     */
    public boolean isValidToken(String token) {
        return !isExpiredToken(token);
    }

    /**
     * Return a new JWT token using user credentials data
     *
     * @param userDetails
     * @return String
     */
    public String getToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
        userDetails.getAuthorities().forEach(authority -> claims.put(CLAIM_KEY_ROLE, authority.getAuthority()));
        claims.put(CLAIM_KEY_CREATED, new Date());

        return generateToken(claims);
    }

    /**
     * Makes parse of JWT token to extract information from it
     *
     * @param token
     * @return Claims
     */
    private Optional<Claims> getClaimsFromToken(String token) {
        try {
            return Optional.ofNullable(Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody());
        } catch (Throwable t) {
            // TODO implements new method to log
            throw t;
        }
    }

    /**
     * Returns expiration date (We'll utilizes currently date)
     *
     * @return Date
     */
    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }

    /**
     * Verifies if JWT token is expired
     *
     * @param token
     * @return boolean
     */
    private boolean isExpiredToken(String token) {
        Date expirationDate = this.getExpirationDateFromToken(token);
        if (expirationDate == null) {
            //TODO Create ERROR Handling
            return false;
        }
        return expirationDate.before(new Date());
    }

    /**
     * Generates a new JWT token with provided data claims
     *
     * @param claims
     * @return String
     */
    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

}
