package com.caioaraujo.minibankproject.security.controllers;

import com.caioaraujo.minibankproject.infra.Response;
import com.caioaraujo.minibankproject.security.dto.TokenDto;
import com.caioaraujo.minibankproject.security.dto.UserJWT;
import com.caioaraujo.minibankproject.security.services.UserJWTService;
import com.caioaraujo.minibankproject.security.utils.JwtParameter;
import com.caioaraujo.minibankproject.security.utils.JwtTokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*")
public class AuthenticationController {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserJWTService userDetailsService;

    /**
     * Generates and return a new JWT Token
     *
     * @param authenticationDto
     * @param result
     * @return
     * @throws AuthenticationException
     */
    @PostMapping
    public ResponseEntity<Response<TokenDto>> generateJwtToken(@Valid @RequestBody UserJWT authenticationDto,
                                                               BindingResult result) {
        Response<TokenDto> response = new Response<>();
        //TODO To refactor the error extraction and logger appender
        if (result.hasErrors()) {
            log.error("Error: {}", result.getAllErrors());
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }

        log.info("Generating JWT token for account number {}.", authenticationDto.getUsername());
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authenticationDto.getUsername(), authenticationDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationDto.getUsername());
        String token = jwtTokenUtil.getToken(userDetails);
        response.setData(new TokenDto(token));

        return ResponseEntity.ok(response);
    }

    /**
     * Generates new token with new expiration date
     * TODO Refactor this method, this method can be codeless
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/refresh")
    public ResponseEntity<Response<TokenDto>> refreshJwtToken(HttpServletRequest request) {
        log.info("Refreshing Jwt Token");
        Response<TokenDto> response = new Response<>();
        Optional<String> token = Optional.ofNullable(request.getHeader(JwtParameter.TOKEN_HEADER));

        if (token.isPresent()) {
            if (token.get().startsWith(JwtParameter.BEARER_PREFIX))
                token = Optional.of(token.get().substring(7));
            if (!jwtTokenUtil.isValidToken(token.get()))
                response.getErrors().add("Token is invalid or already expired.");
            response.setData(new TokenDto(jwtTokenUtil.refreshToken(token.get())));
        } else {
            response.getErrors().add("Token not present.");
        }

        return response.getErrors().isEmpty() ? ResponseEntity.ok(response) : ResponseEntity.badRequest().body(response);
    }

}
