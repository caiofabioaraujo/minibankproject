package com.caioaraujo.minibankproject.controllers;

import com.caioaraujo.minibankproject.entities.dto.SakeTransactionDTO;
import com.caioaraujo.minibankproject.services.AccountOperationTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/operations")
public class OperationsController {

    @Autowired
    private AccountOperationTransactionService transactionService;
    @Autowired
    private SimpMessagingTemplate template;

    @PostMapping(path = "sake", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity sakeOperation(@RequestBody SakeTransactionDTO dto) {
        try {
            transactionService.sakeOperation(dto).whenComplete((transaction, throwable) -> {
                dto.setTransactionIdentifier(transaction.getId());
                dto.setTransactionDate(transaction.getDate());
                template.convertAndSend("/messages", dto);
            });
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/getJson")
    public ResponseEntity<SakeTransactionDTO> getJson() {
        return new ResponseEntity<>(new SakeTransactionDTO(), HttpStatus.I_AM_A_TEAPOT);
    }
}
