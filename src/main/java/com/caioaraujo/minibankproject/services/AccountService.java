package com.caioaraujo.minibankproject.services;

import com.caioaraujo.minibankproject.entities.Account;
import com.caioaraujo.minibankproject.entities.Person;
import com.caioaraujo.minibankproject.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;

import javax.persistence.LockModeType;
import java.math.BigDecimal;

@Service
public class AccountService {

    private AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account getAccountByNumberAccount(String numberAccount){
        return this.accountRepository.getOne(numberAccount);
    }

}
