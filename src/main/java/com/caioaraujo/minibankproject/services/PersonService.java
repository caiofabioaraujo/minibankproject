package com.caioaraujo.minibankproject.services;

import com.caioaraujo.minibankproject.entities.Person;
import com.caioaraujo.minibankproject.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    @Autowired
    private PersonRepository repository;

    public Person getPersonByIdentifyDocument(String identifyDocument) {
        return this.repository.getOne(identifyDocument);
    }

}
