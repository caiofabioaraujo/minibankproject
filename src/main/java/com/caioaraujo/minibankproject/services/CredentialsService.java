package com.caioaraujo.minibankproject.services;

import com.caioaraujo.minibankproject.entities.Credentials;
import com.caioaraujo.minibankproject.repositories.CredentialsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CredentialsService {

    private CredentialsRepository credentialsRepository;

    @Autowired
    public CredentialsService(CredentialsRepository credentialsRepository) {
        this.credentialsRepository = credentialsRepository;
    }

    public Credentials findByNumberAccount(String numberAccount) {
        return credentialsRepository.findByAccount_NumberAccount(numberAccount);
    }
}
