package com.caioaraujo.minibankproject.services;

import com.caioaraujo.minibankproject.entities.*;
import com.caioaraujo.minibankproject.entities.dto.DepositTransactionDTO;
import com.caioaraujo.minibankproject.entities.dto.SakeTransactionDTO;
import com.caioaraujo.minibankproject.entities.dto.TransactionDTO;
import com.caioaraujo.minibankproject.entities.dto.TransferTransactionDto;
import com.caioaraujo.minibankproject.entities.enums.EnumTransactionType;
import com.caioaraujo.minibankproject.repositories.OperationTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

@Service
public class AccountOperationTransactionService {

    @Autowired
    AccountService accountService;
    @Autowired
    PersonService personService;
    @Autowired
    OperationTransactionRepository repository;

    @Async
    @Transactional
    public CompletableFuture<OperationTransaction> sakeOperation(SakeTransactionDTO dto) {
        OperationTransaction transaction = dtoToTransaction(dto);
        transaction = dtoToTransactionSakeOperation(transaction, dto);
        return CompletableFuture.completedFuture(saveTransaction(transaction));
    }

    @Async
    public CompletableFuture<OperationTransaction> depositOperation(DepositTransactionDTO dto) {
        OperationTransaction transaction = dtoToTransaction(dto);
        transaction = dtoToTransactionDepositOperation(transaction, dto);
        return CompletableFuture.completedFuture(transaction);
    }

    @Async
    public CompletableFuture transferOperation(TransferTransactionDto dto) {
        OperationTransaction transactionFrom = dtoToTransactionTransferFromOperation(dtoToTransaction(dto), dto);
        OperationTransaction transactionDestination = dtoToTransactionTransferDestinationOperation(dtoToTransaction(dto), dto);
        CompletableFuture[] futures = {CompletableFuture.completedFuture(saveTransaction(transactionFrom)), CompletableFuture.completedFuture(saveTransaction(transactionDestination))};
        return CompletableFuture.allOf(futures);
    }

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    private OperationTransaction saveTransaction(OperationTransaction transaction) {
        return this.repository.save(transaction);
    }

    @Lock(LockModeType.PESSIMISTIC_READ)
    private Account getAccount(String numberAccount) {
        return this.accountService.getAccountByNumberAccount(numberAccount);
    }

    private Person getPerson(String identifyDocument) {
        return this.personService.getPersonByIdentifyDocument(identifyDocument);
    }

    private OperationTransaction dtoToTransaction(TransactionDTO dto) {
        OperationTransaction transaction = new OperationTransaction();
        Account account = getAccount(dto.getNumberAccount());
        transaction.setDate(new Date());
        transaction.setAccount(account);
        transaction.setOldAccountStatement(account.getStatement());
        transaction.setPerson(getPerson(dto.getIdentifyDocument()));
        AccountOperation operation = new AccountOperation();
        {
            operation.setOperationType(dto.getOperationType());
            operation.setTransaction(transaction);
            transaction.setOperation(operation);
        }
        return transaction;
    }

    private OperationTransaction dtoToTransactionSakeOperation(OperationTransaction transaction, SakeTransactionDTO dto) {
        AccountOperationSake sake = new AccountOperationSake();
        sake.setOperation(transaction.getOperation());
        sake.setAtmRegistration(dto.getAtmRegistration());
        transaction.getOperation().setOperationSake(sake);
        transaction.setTransactionType(dto.getTransactionType());
        transaction.getAccount().setStatement(transaction.getAccount().getStatement().subtract(dto.getTransactionValue()));
        return transaction;
    }

    private OperationTransaction dtoToTransactionDepositOperation(OperationTransaction transaction, DepositTransactionDTO dto) {
        AccountOperationDeposit deposit = new AccountOperationDeposit();
        deposit.setAtmRegistration(dto.getAtmRegistration());
        deposit.setEnvelopeControl(dto.getEnvelopeControl());
        transaction.setTransactionType(dto.getTransactionType());
        transaction.getOperation().setOperationDeposit(deposit);
        transaction.getAccount().setStatement(transaction.getAccount().getStatement().add(dto.getTransactionValue()));
        return transaction;
    }

    private OperationTransaction dtoToTransactionTransferFromOperation(OperationTransaction transaction, TransferTransactionDto dto) {
        return dtoToTransactionTransferOperation(transaction, dto, EnumTransactionType.DEBIT);
    }

    private OperationTransaction dtoToTransactionTransferDestinationOperation(OperationTransaction transaction, TransferTransactionDto dto) {
        return dtoToTransactionTransferOperation(transaction, dto, EnumTransactionType.CREDIT);
    }

    private OperationTransaction dtoToTransactionTransferOperation(OperationTransaction transaction, TransferTransactionDto dto, EnumTransactionType transactionType) {
        if (transactionType.equals(EnumTransactionType.DEBIT)) {
            transaction.setAccount(getAccount(dto.getNumberAccountDestination()));
            transaction.setOldAccountStatement(transaction.getAccount().getStatement());
            transaction.getAccount().setStatement(transaction.getAccount().getStatement().subtract(dto.getTransactionValue()));
        } else {
            transaction.getAccount().setStatement(transaction.getAccount().getStatement().add(dto.getTransactionValue()));
        }
        AccountOperationTransfer transfer = new AccountOperationTransfer();
        transfer.setAccountDestination(transaction.getAccount());
        transfer.setAccountFrom(transaction.getAccount());
        transaction.setTransactionType(transactionType);
        transfer.setOperation(transaction.getOperation());
        transaction.getOperation().setOperationTransfer(transfer);
        return transaction;
    }

}
