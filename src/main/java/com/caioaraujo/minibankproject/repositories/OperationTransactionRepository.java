package com.caioaraujo.minibankproject.repositories;

import com.caioaraujo.minibankproject.entities.OperationTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperationTransactionRepository extends JpaRepository<OperationTransaction, Integer> {
}
