package com.caioaraujo.minibankproject.repositories;

import com.caioaraujo.minibankproject.entities.AccountOperation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountOperationRepository extends JpaRepository<AccountOperation, Integer> {
}
