package com.caioaraujo.minibankproject.repositories;

import com.caioaraujo.minibankproject.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, String> {
}
