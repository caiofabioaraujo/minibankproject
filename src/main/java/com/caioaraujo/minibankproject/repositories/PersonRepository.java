package com.caioaraujo.minibankproject.repositories;

import com.caioaraujo.minibankproject.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, String> {
}
