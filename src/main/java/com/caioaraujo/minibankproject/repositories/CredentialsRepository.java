package com.caioaraujo.minibankproject.repositories;

import com.caioaraujo.minibankproject.entities.Credentials;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CredentialsRepository extends JpaRepository<Credentials, Integer> {
    Credentials findByAccount_NumberAccount(String numberAccount);
}
