create table account (
  number_account VARCHAR(8) UNIQUE,
  statement NUMERIC(19,2),
  active boolean,
  PRIMARY KEY (number_account)
);

create table person (
  identify_document VARCHAR(11) UNIQUE,
  name VARCHAR(150),
  principal_account boolean,
  number_account VARCHAR(8),
  PRIMARY KEY (identify_document),
  FOREIGN KEY (number_account) REFERENCES account(number_account)
);

create table credentials (
  id SERIAL UNIQUE,
  number_account VARCHAR(8) UNIQUE,
  password VARCHAR(50),
  PRIMARY KEY (id),
  FOREIGN KEY (number_account) REFERENCES account(number_account)
);

create table log_credentials (
  id SERIAL UNIQUE,
  date TIMESTAMP,
  id_person VARCHAR(11),
  number_account VARCHAR(8),
  loginSuccesful boolean,
  ip varchar(50),
  PRIMARY KEY (id)
);

create table operation_transaction (
  id SERIAL UNIQUE,
  number_account VARCHAR(8),
  identify_document VARCHAR(11),
  transaction_type VARCHAR(7),
  old_account_statement NUMERIC(19,2),
  date TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (number_account) REFERENCES account(number_account),
  FOREIGN KEY (identify_document) REFERENCES person(identify_document)
);

create table account_operation (
  id SERIAL UNIQUE,
  id_transaction INTEGER UNIQUE,
  operation_type VARCHAR(10),
  operation_value numeric(19,2),
  PRIMARY KEY (id),
  FOREIGN KEY (id_transaction) REFERENCES operation_transaction(id)
);

create table account_operation_deposit (
  id SERIAL UNIQUE,
  id_operation INTEGER UNIQUE,
  envelope_control VARCHAR(50) UNIQUE,
  atm_registration VARCHAR(50),
  PRIMARY KEY (id),
  FOREIGN KEY (id_operation) REFERENCES account_operation(id)
);

create table account_operation_sake (
  id SERIAL UNIQUE,
  id_operation INTEGER UNIQUE,
  atm_registration VARCHAR(50),
  PRIMARY KEY (id),
  FOREIGN KEY (id_operation) REFERENCES account_operation(id)
);

create table account_operation_transfer (
  id SERIAL UNIQUE,
  account_from VARCHAR(8),
  account_destination VARCHAR(8),
  id_operation INTEGER,
  PRIMARY KEY (id),
  FOREIGN KEY (account_from) references account(number_account),
  FOREIGN KEY (account_destination) references account(number_account),
  FOREIGN KEY (id_operation) references account_operation(id)
);

--- insert into account values ('12345678', 2900.10, true);
--- insert into person values ('01234567890', 'Jhon Snow', true, '12345678');
--- insert into credentials values (1, '12345678', 'E10ADC3949BA59ABBE56E057F20F883E');